module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		autoprefixer: {
			options: {
				browsers: ['> 0%'] // more codenames at https://github.com/ai/autoprefixer#browsers
			},
			dev: {
				files: [
					{
						expand: true,
						cwd: 'stylesheets/',
						src: '{,*/}*.css',
						dest: 'stylesheets/'
					}
				]
			}
		},

		watch: {
			scss: {
				files: ['stylesheets/style.scss'],
				tasks: ['compass:dev', 'autoprefixer:dev'],
				options: {
					livereload: true
				}
			},php: {
				files: ['inc/*.inc'],
				tasks: [],
				options: {
					livereload: true
				}
			}
		},

		compass: {

			dev: {
				options: {
					sassDir: 'stylesheets',
					cssDir: 'stylesheets'
				}
			}


		}
	});
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-compass');

	grunt.registerTask('default', ['server']);
	grunt.registerTask('server', ['watch']);
};

