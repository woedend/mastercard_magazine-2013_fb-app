<?php
//error_reporting(-1);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
// set the current edition, this will load the corresponding pages
$protocol = ($_SERVER['HTTPS'] == 'on' ? 'https' : 'http');

define('fan_page', $protocol . '://www.facebook.com/Wexweber');

include_once 'sdk/facebook.php';

$fb_config = array(
	'appId' => '1424297577801319',
	'secret' => '3a869709ab49e6bcf03dfd6edf80062b',
	'cookie' => true,
	'fileUpload' => true
);

$tab_url = fan_page . '/app_' . $fb_config['appId'];
$host = $protocol . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
//strpos($host, $tab_url)
$host = substr($host, 0, strpos($host, 'index.php'));

$fb = new Facebook($fb_config);

$sig = $_REQUEST['signed_request'];

$signed_request = $fb->getSignedRequest();
$page = strip_tags($_GET['page']);
if($signed_request
	&& isset($signed_request['page'])
	&& isset($signed_request['page']['liked'])
	&& $signed_request['page']['liked'] == 1
) {
	$like_gate = FALSE;
} else {
	$like_gate = TRUE;
}
?><!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" xmlns:og="http://ogp.me/ns#"
	  dir="ltr">
<head>
	<meta charset="utf-8"/>
	<title>SelectedBy Mastercard</title>
	<script>
		 if (window.top.frames.length < 1) {
		 window.top.location.href = '<?php echo $tab_url; ?>';
		 }

	</script>


	<meta name="description" content=""/>
	<meta name="author" content="W!Dev"/>
	<base href="<?php echo $host; ?>"/>
	<meta property="fb:app_id" content="<?php echo $fb_config['appId']; ?>"/>
	<link href='//fonts.googleapis.com/css?family=Signika+Negative:300,400' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="stylesheets/style.css"/>

	<!--[if lt IE 9]>
	<script src="bower_components/html5shiv/dist/html5shiv.js"></script>
	<![endif]-->


</head>
<body class="">
<div id="fb-root"></div>
<div class="container">
	<?php if($like_gate) : ?>
			<div class="likegate">
		</div>
	<?php endif; ?>
	<?php
	$array = array(1 => 'georgina', 2 => 'vincenteverts', 3 => 'misslipgloss');
	$key = array_search($page, $array);

	if(!$key) {
		$page = "homepage";
	}
	?>
	<nav class="menu">
		<a href="?&amp;signed_request=<?php echo $sig; ?>" class="menu_logo"></a>
		<ul class="menu-items">
			<li class="menu-item menu-item-active">
				<a class="menu-item-link<?php if($key == 1) echo " active"; ?>"
				   href="?page=georgina&amp;signed_request=<?php echo $sig; ?>">
					Georgina Verbaan
				</a>
			</li>
			<li class="menu-item">
				<a class="menu-item-link<?php if($key == 2) echo " active"; ?>"
				   href="?page=vincenteverts&amp;signed_request=<?php echo $sig; ?>">
					Vincent Everts
				</a>
			</li>
			<li class="menu-item">
				<a class="menu-item-link<?php if($key == 3) echo " active"; ?>"
				   href="?page=misslipgloss&amp;signed_request=<?php echo $sig; ?>">
					Miss Lipgloss
				</a>
			</li>
			<li class="menu-item">
				<a class="menu-item-home"
				   href="?&amp;signed_request=<?php echo $sig; ?>"></a>
			</li>
		</ul>
	</nav>
	<?php include('inc/' . $page . '.inc'); ?>
</div>
<script>
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-7992452-8']);
	_gaq.push(['_trackPageview']);

	(function () {
		var ga = document.createElement('script');
		ga.type = 'text/javascript';
		ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0];
		s.parentNode.insertBefore(ga, s);
	})();
	function t(action, label) {
<!--		_gaq.push(['_trackEvent', '--><?php //echo EDITION; ?><!--', action, label])-->
	}
	window.fbAsyncInit = function () {
		setTimeout(function () {
			FB.Canvas.setAutoGrow();
			FB.Canvas.scrollTo(0, 1);
		}, 250);
		FB.Event.subscribe('edge.create', function (response) {
			_gaq.push(["_trackEvent", "Liked", response]);
			window.top.location.href = '<?php echo $tab_url; ?>';
		});
		FB.Event.subscribe('edge.remove', function (response) {
				_gaq.push(["_trackEvent", "Unliked", response]);
				window.top.location.href = '<?php echo $tab_url; ?>';
			}
		)
	};
	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/nl_NL/all.js#xfbml=1&appId=<?php echo $fb_config['appId']; ?>";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
</script>
<script src="http://localhost:35729/livereload.js"></script>
<script src="bower_components/jquery/jquery.min.js"></script>
<script src="bower_components/neatshowjs/neatshow.min.js"></script>


</body>
</html>