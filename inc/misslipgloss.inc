<section class="intro-wrapper" style="background:url('images/misslipgloss/header.jpg')">
	<div class="intro">
		<small>selected by</small>
		<h1 class="intro-title">
			Miss Lipgloss
		</h1>

		<div class="intro-body">
			<p>Op haar zestiende startte Cynthia Schultz haar beauty blog Miss Lipgloss. We kunnen gerust zeggen dat deze
				hobby uit de hand gelopen is, want inmiddels is zij de meest invloedrijke beauty blogger van Nederland.
				Dagelijks schrijft Cynthia Schultz over alles wat haar boeit op het gebied van beauty en lifestyle. Ben
				jij op zoek naar een beauty product voor onder de kerstboom? Bekijk dan Cynthia’s niet te missen
				tips.</p>
		</div>

		<div class="intro-button">
			<a href="javascript:void(0);" onclick="FB.Canvas.scrollTo(0, 411);" class="button purple">Bekijk</a>
		</div>

	</div>
</section>

<section id="grid">
	<div class="row row_1">

		<!--LANCOME-->
		<div class="column_1 box box_1 box_high" style="background: url('images/misslipgloss/2.jpg')">

			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Lancome La Vie est Belle</h2>

				<div class="grid-pane-body">
					<p>
						Lancome La Vie est Belle is sinds een tijdje een van mijn favoriete geuren. Zacht, poederig en
						een beetje bloemig. En hoe prachtig is de fles? De Eau Legere is trouwens een lichtere,
						luchtigere versie van de geur.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.debijenkorf.nl/lancome-la-vie-est-belle-2125090034" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>

		<!--CREAM-->
		<div class="column_2 box box_2" style="background: url('images/misslipgloss/1.jpg')">
			<div class="grid-pane grid-pane-right grid-pane-drawer">
				<h2 class="grid-pane-title">Guerlain BB Cream</h2>

				<div class="grid-pane-body">
					<p>Ik ben verzot op de BB Cream van Guerlain, de Guerlain Lingerie de Peau Beauty Booster. Ik ben
						inmiddels toe aan mijn tweede tube! Deze BB Cream slash foundation is geweldig mooi dekkend,
						geeft een natuurlijke effect en blijft de hele dag op zijn plek. </p>
				</div>
				<div class="grid-button">
					<a href="http://www.douglas.nl/douglas/Make-up-Teint-Foundation-Guerlain-Gezichtsmake-up-Lingerie-de-Peau-Beauty-Booster-SPF30_product_3000051915.html" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row row_2">

		<!--MAC-->
		<div class="column_2 box box_1" style=" background: url('images/misslipgloss/5.jpg')">
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">MAC Lip Erase</h2>

				<div class="grid-pane-body">
					<p>
						Al jaren een favoriet product van mij: Lip Erase van MAC. Lip Erase dep je op je lippen zodat ze
						helemaal huidskleurig worden. Ideaal als je houdt van de nude look, maar ook handig onder
						lipsticks. De lipstick wordt intenser en echter van kleur en blijft ook langer zitten.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.maccosmetics.nl/product/shaded/8680/1625/Pro-Products/Lippen/Lip-Erase/index.tmpl" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>

		<!--ZOEVA-->
		<div class="column_3 box box_1" style="background: url('images/misslipgloss/3.jpg')">
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Zoeva Kwasten</h2>

				<div class="grid-pane-body">
					<p>De kwasten van de Duitse webshop Zoeva zijn favoriet. Heel erg betaalbaar en kwalitatief
						geweldig. Mijn nummer één is de buffer brush, deze gebruik ik om mijn foundation of BB Cream mee
						aan te brengen. Er is erg veel keuze bij webshop Zoeva, een aanrader!</p>
				</div>
				<div class="grid-button">
					<a href="http://www.zoeva-shop.de/en/brushes-accessories/" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row row_3">

		<!--KORTING-->
		<div class="column_1 box box_1" style="background: url('images/misslipgloss/korting.jpg')">
			<div class="grid-button">
				<a href="https://www.haalmeeruitjecard.nl/games/slimvoordeel/index.jsp#&slider1=2" target="_blank" class="button orange">Bekijk aanbieding</a>
			</div>
		</div>

		<!--NARS-->
		<div class="column_2 box box_2" style="background: url('images/misslipgloss/4.jpg')">
			<div class="grid-pane grid-pane-left grid-pane-drawer">
				<h2 class="grid-pane-title">NARS Albatross</h2>

				<div class="grid-pane-body">
					<p>Ben je nog op zoek naar een warme, glanzende highlighter? Albatross van NARS is dé perfecte
						highlighter. Hij geeft een geelgouden glans aan je jukbeenderen, maar je kan hem ook bij je
						binnenste ooghoeken of onder je wenkbrauwen gebruiken. Gaat ontzettend lang mee!</p>
				</div>
				<div class="grid-button">
					<a href="http://www.asos.com/Nars/NARS-Highlighting-Blush-Powder/Prod/pgeproduct.aspx?iid=1569263&SearchQuery=nars%20blush&sh=0&pge=0&pgesize=36&sort=-1&clr=Albatross" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>

		</div>
	</div>
</section>

