<section class="intro-wrapper" style="background: url('images/vincenteverts/header.jpg')">
	<div class="intro">
		<small>selected by</small>
		<h1 class="intro-title">
			Vincent Everts
		</h1>

		<div class="intro-body">
			<p>Vincent Everts is een van de belangrijkste Nederlandse trendwatchers op het gebied van technologie van dit
				moment. Waar de nieuwste technologische innovaties en gadgets zijn, is Vincent ook te vinden. Wil jij
				weten welke gadgets je deze kerst aan moet schaffen om in 2014 voorop te lopen? Bekijk dan Vincent’s
				gadgets van dit moment!</p>
		</div>
		<div class="intro-button">
			<a href="javascript:void(0);" onclick="FB.Canvas.scrollTo(0, 411);" class="button purple">Bekijk</a>
		</div>

	</div>
</section>

<section id="grid">
	<div class="row row_1">

		<!--PEBBLE-->
		<div class="column_1 box box_1 box_high" style="background: url('images/vincenteverts/5.jpg')">
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Pebble</h2>

				<div class="grid-pane-body">
					<p>
						De Pebble is een horloge als afstandsbediening voor je smartphone. Met deze gadget kun je o.a.
						de muziek op je telefoon besturen, volume harder of zachter zetten of de nummers selecteren. Er
						zijn duizenden apps die je op je horloge kunt installeren.
					</p>
				</div>
				<div class="grid-button">
					<a href="https://getpebble.com" target="_blank" class="button orange">Bestel</a>
				</div>
			</div>
		</div>

		<!--LEAP MOTION-->
		<div class="column_2 box box_2" style="background: url('images/vincenteverts/2.jpg')">
			<div class="grid-pane grid-pane-right grid-pane-drawer">
				<h2 class="grid-pane-title">Leap Motion 3D</h2>

				<div class="grid-pane-body">
					<p>
						Met de Leap Motion 3D controller bedien je computers met je handen. Nadat je het kleine doosje
						met 2 camera’s aansluit kan je veel gebruikte commando’s simpel activeren, door met je handen te
						zwaaien.

					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.leapmotion.com" target="_blank" class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row row_2">
		<div class="column_2 box box_1" style="background:  url('images/vincenteverts/1.jpg')">

			<!--NEXUS 5-->
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Nexus 5</h2>

				<div class="grid-pane-body">
					<p>
						Natuurlijk is de Samsung Galaxy S4 een mooi toestel maar die zit vol met software die je
						eigenlijk niet wilt hebben. Google heeft nu zelf een supersnel Android toestel op de markt
						gebracht met een flink 5 inch full HD scherm. Top toestel voor een klein prijsje.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.belsimpel.nl/lg/lg-nexus-5-white/overzicht" target="_blank" class="button orange">Bestel</a>
				</div>
			</div>
		</div>
		<!--NEXUS 7-->
		<div class="column_3 box box_1" style="background: url('images/vincenteverts/3.jpg'); ">

			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Asus Google Nexus 7</h2>

				<div class="grid-pane-body">
					<p>
						In 2013 zijn meer dan 60% van alle Tablets Android. Je hebt meer keuze in grootte, geheugen,
						prijs en prestatie. De beste tablet levert Google nu zelf en dan heb je een 7 inch super snelle
						hoogwaardige device met een full HD Scherm en 32 GB intern.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.wehkamp.nl/elektronica/ipad-tablets/ipad-tablets/asus-google-nexus-7-(2013)-32gb-7-inch-tablet/C26_6F0_6F0_781700/?BC=GOA&cm_mmc_o=7BBTkwCjC-kTwFwwECVyBpAgfkblfbETzplCjCbVzpwEfz_kwflCjCIovIii&BAC=ALG" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>

		</div>
	</div>
	<div class="row row_3">
		<!--DELL KORTING-->
		<div class="column_1 box box_1" style="background: url('images/vincenteverts/korting.jpg');">
			<div class="grid-button">
				<a href="https://www.haalmeeruitjecard.nl/games/slimvoordeel/index.jsp#&slider1=3" target="_blank" class="button orange">Bekijk aanbieding</a>
			</div>


		</div>
		<!--TESLA-->
		<div class="column_2 box box_2" style="background: url('images/vincenteverts/4.jpg')">
			<div class="grid-pane grid-pane-left grid-pane-drawer">
				<h2 class="grid-pane-title">Tesla</h2>

				<div class="grid-pane-body">
					<p>
						Tesla staat voor de toekomst van auto rijden. Een klassiek ontworpen elektrische auto die aan de
						voorkant lijkt op een Aston Martin en van achteren op een Jaguar. Als je de auto binnengaat valt
						vooral het 17 inch scherm op, waarmee je alles bedient.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.teslamotors.com/nl_NL/models/design" target="_blank" class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</div>
</section>

