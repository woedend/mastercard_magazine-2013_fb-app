<section class="intro-wrapper" style="background:url('images/georgina/header.jpg')">
	<div class="intro">
		<small>selected by</small>
		<h1 class="intro-title">
			Georgina Verbaan
		</h1>

		<div class="intro-body">
			<p>Het succes lacht actrice Georgina Verbaan dit jaar toe. Eerder dit jaar won ze op het Nederlands Film
				Festival een Gouden Kalf voor haar bijrol in de tragiekomedie De Marathon en even daarvoor was zij de
				voor velen onverwachte ster van de serie ’t Schaep. Met recht mag zij zich nu een van de beste actrices
				van Nederland noemen. Georgina koos voor ons een aantal verrassende producten. Bekijk haar inspirerende
				selectie.
			</p>
		</div>
		<div class="intro-button">
			<a href="?page=georgina&amp;signed_request=<?php echo $sig; ?>" class="button purple">Bekijk</a>
		</div>
	</div>
</section>

<section id="grid">
	<section class="row row_1">

		<!--VINCENT EVERTS-->
		<div class="column_1 box box_2" style="background: url('images/home/1.jpg');">
			<div class="grid-pane grid-pane-right grid-pane-drawer">
				<small>Vincent Everts</small>
				<h2 class="grid-pane-title">Gadgets</h2>

				<div class="grid-pane-body">
					<p>
						Vincent Everts is een van de belangrijkste Nederlandse trendwatchers op het gebied van
						technologie van dit moment. Waar de nieuwste technologische innovaties en gadgets zijn, is
						Vincent ook te vinden.
					</p>
				</div>
				<div class="grid-button">
					<a href="?page=vincenteverts&amp;signed_request=<?php echo $sig; ?>"
					   class="button purple">Bekijk</a>
				</div>
			</div>
		</div>
		<!--MISS LIPGLOSS-->
		<div class="column_3 box box_1 box_high" style="background: url('images/home/2.jpg');">
			<div class="grid-pane grid-pane-drawer">
				<small>Miss Lipgloss</small>
				<h2 class="grid-pane-title">Beauty</h2>

				<div class="grid-pane-body">
					<p>
						Op haar zestiende startte Cynthia Schultz haar beauty blog Miss Lipgloss. We kunnen gerust zeggen
						dat deze hobby uit de hand gelopen is, want inmiddels is zij de meest invloedrijke beauty
						blogger van Nederland.
					</p>
				</div>
				<div class="grid-button">
					<a href="?page=misslipgloss&amp;signed_request=<?php echo $sig; ?>" class="button purple">Bekijk</a>
				</div>
			</div>

		</div>
	</section>
	<section class="row row_2">
		<div class="column_1 box box_1" style="background: url('images/home/5.jpg')">
			<div class="grid-pane grid-pane-drawer">
				<small>Gadgets</small>
				<h2 class="grid-pane-title">Tesla</h2>

				<div class="grid-pane-body">
					<p>
						Tesla staat voor de toekomst van auto rijden. Een klassiek ontworpen elektrische auto die aan de
						voorkant lijkt op een Aston Martin en van achteren op een Jaguar. Als je de auto binnengaat valt
						vooral het 17 inch scherm op, waarmee je alles bedient.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.teslamotors.com/nl_NL/models/design" target="_blank" class="button orange">Bestel</a>
				</div>
			</div>
		</div>
		<div class="column_2 box box_1" style="background: url('images/home/3.jpg')">
			<div class="grid-pane grid-pane-drawer">
				<small>Beauty</small>
				<h2 class="grid-pane-title">Lancome La Vie est Belle</h2>

				<div class="grid-pane-body">
					<p>
						Lancome La Vie est Belle is sinds een tijdje een van mijn favoriete geuren. Zacht, poederig en
						een beetje bloemig. En hoe prachtig is de fles? De Eau Legere is trouwens een lichtere,
						luchtigere versie van de geur.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.debijenkorf.nl/lancome-la-vie-est-belle-2125090034" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</section>
	<section class="row row_3">
		<div class="column_1 box box_1" style="background: url('images/vincenteverts/korting.jpg');">
			<div class="grid-button">
				<a href="https://www.haalmeeruitjecard.nl/games/slimvoordeel/index.jsp#&slider1=3"  target="_blank" class="button orange">Bekijk aanbieding</a>
			</div>


		</div>
		<div class="column_2 box box_2" style="background: url('images/home/4.jpg');">
			<div class="grid-pane grid-pane-left grid-pane-drawer">
				<small>Gadgets</small>
				<h2 class="grid-pane-title">Leap Motion 3D</h2>

				<div class="grid-pane-body">
					<p>
						Met de Leap Motion 3D controller bedien je computers met je handen. Nadat je het kleine doosje
						met 2 camera’s aansluit kan je veel gebruikte commando’s simpel activeren, door met je handen te
						zwaaien.

					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.leapmotion.com " target="_blank" class="button orange">Bestel</a>
				</div>
			</div>

		</div>
	</section>
</section>

