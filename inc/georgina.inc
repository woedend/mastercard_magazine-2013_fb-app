<section class="intro-wrapper" style="background:url('images/georgina/header.jpg')">
	<div class="intro">
		<small>selected by</small>
		<h1 class="intro-title">
			Georgina Verbaan
		</h1>

		<div class="intro-body">
			<p>Het succes lacht actrice Georgina Verbaan dit jaar toe. Eerder dit jaar won ze op het Nederlands Film
				Festival een Gouden Kalf voor haar bijrol in de tragiekomedie De Marathon en even daarvoor was zij de
				voor velen onverwachte ster van de serie ’t Schaep. Met recht mag zij zich nu een van de beste actrices
				van Nederland noemen. Georgina koos voor ons een aantal verrassende producten. Bekijk haar inspirerende
				selectie. </p>
		</div>
		<div class="intro-button">
			<a href="javascript:void(0);" onclick="FB.Canvas.scrollTo(0, 411);" class="button purple">Bekijk</a>
		</div>
	</div>
</section>

<section id="grid">
	<div class="row row_1">

		<!--VINTAGE-->
		<div class="column_1  box box_1 box_high" style="background: url('images/georgina/2.jpg') ">

			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Bannou</h2>

				<div class="grid-pane-body">
					<p>
						Prachtige webshop met vintage en vintage inspired kleding. Van het duurzame en sociaal
						verantwoorde merk Bannou van eigenaresse Faranak word ik persoonlijk heel blij. Classy and fun!
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.ilovevintage.nl/bannou/the-petrol-zoe-dress-by-bannou.html" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>

		</div>

		<!--SLIP-->
		<div class="column_2 box box_2" style="background: url('images/georgina/1.jpg') ">
			<div class="grid-pane grid-pane-right grid-pane-drawer">
				<h2 class="grid-pane-title">Pleasurements</h2>

				<div class="grid-pane-body">
					<p>
						Ooh la la! Chantal Thomass, La Perla, Pleasure State, Bordelle, Fraulein Kink en nog véél meer.
						Luxueus en verleidelijk. Kleine edoch aardige collectie latex. Ook voor uw designer maskers,
						zwepen en bondagesetjes...
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.pleasurements.com/nl/la-perla-lingerie-marchesa-terracotta-highwaist-garter-brief"
					   target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>


		</div>
	</div>
	<div class="row row_2">

		<!--MOISTURIZER-->
		<div class="column_2 box box_1" style="background: url('images/georgina/3.jpg') ">
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Dr. Jetske Ultee</h2>

				<div class="grid-pane-body">
					<p>
						Als je nog niet van Dr. Jetske Ultee gehoord hebt smeer je waarschijnlijk rommel op je gezicht. Er
						zitten alleen maar werkzame stoffen in haar producten en - belangrijk - niets is op dieren
						getest.

					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.uncover-skincare.nl/cgi/shop.cgi/moisturizer/?site=uncover-skincare&details=000176&conpag=p000146 "
					   target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>

		<!--ANTHROPOLOGIE-->
		<div class="column_3 box box_1" style="background:  url('images/georgina/4.jpg') ">
			<div class="grid-pane grid-pane-drawer">
				<h2 class="grid-pane-title">Anthropologie</h2>

				<div class="grid-pane-body">
					<p>
						Kleding, schoenen, de prachtigste spullen voor in je huis; ik wil werkelijk alles hebben. Het
						beddengoed en de katoenen of linnen douchegordijnen, het is zo mooi allemaal. Weer dat moderne
						vintage gevoel waar ik zo van houd.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.anthropologie.eu/anthro/product/home-bathroom/7534601440001.jsp" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>
		</div>
	</div>
	<div class="row row_3">
		<!--KORTING-->
		<div class="column_1 box box_1" style="background: url('images/georgina/korting.jpg') ">
			<div class="grid-button">
				<a href="https://www.haalmeeruitjecard.nl/games/slimvoordeel/index.jsp#&slider1=1" target="_blank" class="button orange">Bekijk aanbieding</a>
			</div>
		</div>
		<!--KATTENBOOT-->
		<div class="column_2 box box_2" style="background:  url('images/georgina/5.jpg') ">
			<div class="grid-pane grid-pane-left grid-pane-drawer">
				<h2 class="grid-pane-title">De Poezenboot</h2>

				<div class="grid-pane-body">
					<p>
						Het is de meest bijzondere boot van Amsterdam: drijvend asiel De Poezenboot. Het bestaat bij de
						gratie van vrijwilligers en donateurs met een groot poezenhart. Je kan hier direct een poes
						kopen maar ook gerust een donatie geven.
					</p>
				</div>
				<div class="grid-button">
					<a href="http://www.poezenboot.nl" target="_blank"
					   class="button orange">Bestel</a>
				</div>
			</div>

		</div>
	</div>
</section>

